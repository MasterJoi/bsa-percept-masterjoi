package bsa.java.concurrency.image;

import bsa.java.concurrency.fs.FileSystemService;
import bsa.java.concurrency.hashing.DHasherService;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Service
public class ImageService {
    private static final Logger logger = LoggerFactory.getLogger(ImageService.class);

    @Autowired
    private DHasherService dHasherService;

    @Autowired
    private FileSystemService fileSystemService;

    @Autowired
    private ImageRepository imageRepository;

    public void batchUploadImages(MultipartFile[] files) {
        Arrays.stream(files)
                .parallel()
                .forEach( multipartFile -> {
                    byte[] bytes = fileSystemService.getBytes(multipartFile);
                    UUID imageID = UUID.randomUUID();

                    CompletableFuture<Long> hash = CompletableFuture.supplyAsync(() -> dHasherService
                            .calculateHash(bytes));
                    logger.info(multipartFile.getOriginalFilename() + " hash was created.");

                    CompletableFuture<String> filepath = fileSystemService.saveFile(bytes, imageID);
                    logger.info(multipartFile.getOriginalFilename() + " image was downloaded.");

                    CompletableFuture<Void> combinedFuture = CompletableFuture.allOf(hash, filepath);
                    combinedFuture.thenAccept(image -> imageRepository
                            .save(new Image(imageID, hash.join(), filepath.join()))).exceptionally(ex -> {
                        throw new RuntimeException("Can`t create record about image: " + multipartFile.getName());
                    });
                    logger.info(multipartFile.getOriginalFilename() + " record was created.");
                });
    }

    public List<SearchResultDTO> searchMatches(MultipartFile file, double threshold) {
        if(threshold < 0 || threshold > 1) {
            throw new InputMismatchException("Incorrect threshold input!");
        } else {
            byte[] bytes = fileSystemService.getBytes(file);
            UUID imageID = UUID.randomUUID();

            Long hash = dHasherService.calculateHash(bytes);
            logger.info(file.getOriginalFilename() + " hash was created.");

            List<SearchResultDTO> searchResultDto = imageRepository.getSearchResultDto(hash, threshold);

            if(searchResultDto.isEmpty()) {
                CompletableFuture<String> filepath = fileSystemService.saveFile(bytes, imageID);
                logger.info(file.getOriginalFilename() + " image was downloaded.");

                filepath.thenAccept(image -> imageRepository
                        .save(new Image(imageID, hash, filepath.join()))).exceptionally(ex -> {
                    throw new RuntimeException("Can`t create record about image: " + file.getName());
                });
                logger.info(file.getOriginalFilename() + " record was created.");
            }
            return searchResultDto;
        }
    }

    public void deleteById(UUID imageId) {
        fileSystemService.deleteImageById(imageId);
        imageRepository.deleteById(imageId);
        logger.info("Image: " + imageId + ".jpg was deleted.");
    }

    public void deleteAll() {
        fileSystemService.deleteAllImages();
        imageRepository.deleteAll();
        logger.info("All images were deleted.");
    }
}
