package bsa.java.concurrency.image;

import bsa.java.concurrency.image.dto.SearchResultDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ImageRepository extends JpaRepository<Image, UUID> {

    @Query(value = "SELECT CAST(i.id AS VARCHAR ) AS imageId," +
            "((1 - ((array_length(string_to_array( CAST( CAST((i.hash # :hash) AS BIT(64)) AS TEXT), '1'), 1) - 1) / 64.0)) * 100) AS matchPercent," +
            "i.url AS imageUrl FROM Images i " +
            "WHERE (1 - ((array_length(string_to_array( CAST( CAST((i.hash # :hash) AS BIT(64)) AS TEXT), '1'), 1) - 1) / 64.0)) >= :threshold " +
            "ORDER BY matchPercent DESC ", nativeQuery = true)
    List<SearchResultDTO> getSearchResultDto(Long hash, double threshold);
}
