package bsa.java.concurrency.image;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "images")
public class Image {
    @Id
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column
    private Long hash;

    @Column
    private String url;
}
