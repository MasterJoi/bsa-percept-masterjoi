package bsa.java.concurrency.fs;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public interface FileSystem {
    CompletableFuture<String> saveFile(byte[] bytes, UUID id);
}
