package bsa.java.concurrency.fs;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Service
public class FileSystemService implements FileSystem {

    @Value("${path.rootDirectory}")
    private String rootPath;
    @Value("${server.port}")
    private String port;

    @Override
    public CompletableFuture<String> saveFile(byte[] file, UUID id) {
        return CompletableFuture.supplyAsync(() -> {
            createRootDirectory();
            File imageFile = new File(rootPath + "/" + id + ".jpg");

            try (OutputStream fileOutputStream = new FileOutputStream(imageFile)) {
                fileOutputStream.write(file);
                return toImageURL(imageFile.getPath());
            } catch (IOException e) {
               throw new RuntimeException("Can`t download image : " + imageFile.getName() + " to directory!");
            }
        });
    }

    private String toImageURL(String path) {
        try {
            String imageURL = "http://" + InetAddress.getByName("localhost").getHostAddress() +
                    ":" + port + "/" + path;
            return imageURL.replace("\\", "/");
        } catch (UnknownHostException e) {
            throw new Error("Can`t find host IP address!");
        }
    }

    private void createRootDirectory() {
        try {
            Files.createDirectories(Paths.get(rootPath));
        } catch (IOException e) {
            throw new RuntimeException("Can`t create root directory!");
        }
    }

    public byte[] getBytes(MultipartFile multipartFile) {
        try {
            return multipartFile.getBytes();
        } catch (IOException e) {
            throw new RuntimeException("Can`t read file: " + multipartFile.getName());
        }
    }

    public void deleteImageById(UUID imageId) {
        Path imagePath = FileSystems.getDefault().getPath(rootPath + "/" + imageId + ".jpg");
        try {
            Files.deleteIfExists(imagePath);
        } catch (IOException ex) {
            throw new RuntimeException("Can`t delete file: " + imageId + ".jpg !");
        }
    }

    public void deleteAllImages() {
        File rootDirectory = new File(rootPath);
        try {
            FileUtils.cleanDirectory(rootDirectory);
        } catch (Exception ex) {
            throw new RuntimeException("Can`t delete root directory content!");
        }
    }
}
